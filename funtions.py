def hello(name="Person"):
    print("Hello ", name)


hello("John")
hello()


def add(n1, n2):
    n3 = n1+n2
    return n3


print(add(10, 30))

# Funtion lambda


sum = lambda n1, n2 : n1+n2

print(sum(5,5))

