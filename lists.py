demoList = [1, "hello", 1.34, True, [1, 2, 3]]
colorList = ["red",  "green", "blue"]
numList = list((1, 2, 3, 4))
print(type(numList))
print(numList)

r = list(range(1,10))
n = len(colorList)
print(colorList[0])
print(colorList[-1])
print(n)
print(r)
print('red' in colorList)
print(colorList)
colorList[0] = 'yelow'
print(colorList)
colorList.append(('brown')) 
# Append Solo agrega un elemento Ej List, tupla pero no dos elementos de una lista
colorList.extend(['violet','white'])
#Extend Puedo agregar una tupla o list y los toma como elementos de la lista
print(colorList)
# print(dir(colorList)) Metodos
colorList.insert(1, 'black')
colorList.insert(len(colorList), 'pink')
print(colorList)

colorList.pop()
# Elimina el ultimo o el indice [CualquierNumero]
print(colorList)

colorList.remove('green')
# Por el nombre
print(colorList)
# colorList.clear()
# # elimina todo


colorList.sort()
# ordena alfabeticamente
print(colorList)

colorList.sort(reverse=True)
# Ordena alreves
print(colorList)


print(colorList.index('yelow'))