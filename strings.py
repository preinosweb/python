mystr = "hello World"

# print(dir(mystr)) operaciones de strings con dir
print("My firts program in python is -> " + mystr)
print(f"My firts program in python is -> {mystr}")
print("My firts program in python is -> {0}".format(mystr))

print(mystr.upper())
print(mystr.lower())
print(mystr.capitalize())

print(mystr.replace('hello', 'bye').upper())
print(mystr.count('l'))

print(mystr.startswith('hello'))
print(mystr.endswith('World'))

print(mystr.split())
print(mystr.split('o'))

print(mystr.find('n'))
print(mystr.find('h'))
print(mystr.find('o'))

print(len(mystr))
print(mystr.index('e'))

print(mystr.isnumeric())
print(mystr.isalpha())


print(mystr[0])
print(mystr[1])
print(mystr[2])
print(mystr[3])
print(mystr[4])

print(mystr[-5])
print(mystr[-4])
print(mystr[-3])
print(mystr[-2])
print(mystr[-1])