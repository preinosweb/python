x = (1)
print(x)
print(type(x))


y = tuple((1,))
print(y)
print(type(y))

# # Tupla no acepta cambio valores
# y[0] = 2
#
# del x
# print(x)
# print(dir(x))

locations = {
    (31.1234, 56.678): "Tokio",
    (43.5676, 98.542): "Florida"
}

print(locations)
