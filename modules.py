# python modules


import datetime 
from datetime import timedelta , date

print(datetime.date.today())
print(datetime.timedelta(minutes=70))

print(timedelta(minutes=70))
print(date.today())

# My modules

import fmath
from fmath import add, substract

print(fmath.add(1,1))
print(fmath.substract(2,1))

print(add(1,1))
print(substract(2,1))

# pypi modules (pypi.org) Ej Django (Framework Web), flask (Web), tekinfer(UI Escritorio)