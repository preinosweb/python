#Strings
print("Hello World")
print(type("Hello World"))

#Integer
print(10+6)
print(type(10+30))
#Float
print(type(30.5))

#Boolean
print("boolean",True)
print(type(False))

#List (Datos que pueden cambiar)
print(["List: ", "hello", "Bye", True, False, 10, 10.5])
print(type([1,2,3]))

#Tuples -(Datos que no pueden cambiar)
print('Tuples: ',10,20,30,40)
print(type((10,20,30)))

#Dictionaries
print({
    "Name " : "Ryan",
    "LastName " : "Rey",
    "NickName "  : "fast"
})
print(type({
    "Name " : "Ryan",
    "LastName " : "Rey",
    "NickName "  : "fast"
}))

#None
print(type(None))