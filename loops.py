numbers = [1, 2, 3]

x = int(len(numbers))
print(type(x))
print("---------------")
for number in numbers:
    print(number)
    if number == 2:
        break
        # continue

print("---------------")
for letter in "hello":
    print(letter)


print("---------------")
x = 1
while x <= 10:
    print(x)
    x = x + 1
    
