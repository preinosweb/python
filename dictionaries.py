product = {
    "name": "book",
    "quantity": 3,
    "price": 4.99
}

person = {
    "first_name": "ryan",
    "last_name": "ray"
}

print(type(person))
print(type(product))

print(person.keys())
print(person.items())

# person.clear()
# del person

products = [
    {"name": 'book', "price": 10.99},
    {"name": 'laptop', "price": 1000}
]

print(products)